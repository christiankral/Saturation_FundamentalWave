using PyPlot

zer_nom = 5
inf_nom = 0.2
Lnom = 1
Lzer = zer_nom * Lnom
Linf = inf_nom * Lnom

close("all")
i = collect(0:0.001:20)
for Ipar in collect(0.25:0.25:1.5)
  figure(num=1)
  Lact = Linf .+ (Lzer - Linf) * atan.(i/Ipar) ./ (i/Ipar)
  plot(i,Lact / Lnom)
  figure(num=2)
  Psi = Linf*i .+ (Lzer - Linf)*Ipar*atan.(i/Ipar)
  plot(i,Psi)
end
