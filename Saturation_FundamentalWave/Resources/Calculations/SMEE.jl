println("SMEE")
m = 3
VsNominal = 100
effectiveStatorTurns = 10
fsNominal = 50
Lmd = 0.00477465
IeOpenCircuit = 1
turnsRatio = sqrt(2)*VsNominal/(2*pi*fsNominal*Lmd*IeOpenCircuit)
println("turnsRatio = ", turnsRatio)
Ne = effectiveStatorTurns*turnsRatio*m/2
println("Ne = ", Ne)
V_m = (2 / pi) * Ne * IeOpenCircuit
println("V_m = ",V_m)
# Double check after substituting IeOpenCircuit and Ne
V_m = (VsNominal*effectiveStatorTurns*m)/(sqrt(2)*Lmd*fsNominal*pi^2)
println("V_m = ",V_m)
# Equivalent stator current
Is = (pi / 2) * (2 / m) * V_m / (sqrt(2) * effectiveStatorTurns)
println("Is = ", Is)
# Double check calculation
V_m = sqrt(2)*(2.0/pi)*effectiveStatorTurns*Is*m/2
println("V_m = ",V_m)
