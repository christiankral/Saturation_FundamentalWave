within Saturation_FundamentalWave.QuasiStatic.Examples.SynchronousMachines;
model SMPM_OpenCircuit
  "Test example: PermanentMagnetSynchronousMachine with inverter"
  extends Modelica.Icons.Example;
  import Modelica.Constants.pi;
  parameter Integer m=3 "Number of phases";
  output Modelica.SIunits.Voltage Vtr=potentialSensor.phi[1]
    "Transient voltage";
  output Modelica.SIunits.Voltage Vqs=potentialSensorQS.abs_y[1]
    "QS voltage";
  Saturation_FundamentalWave.QuasiStatic.SynchronousMachines.SM_ElectricalExcited
    smpmQS(
    p=smeeData.p,
    fsNominal=smeeData.fsNominal,
    TsRef=smeeData.TsRef,
    Jr=smeeData.Jr,
    Js=smeeData.Js,
    frictionParameters=smeeData.frictionParameters,
    statorCoreParameters=smeeData.statorCoreParameters,
    strayLoadParameters=smeeData.strayLoadParameters,
    useSaturation=true,
    zer_nomd=2,
    inf_nomd=0.1,
    useDamperCage=smeeData.useDamperCage,
    Lrsigmad=smeeData.Lrsigmad,
    Lrsigmaq=smeeData.Lrsigmaq,
    Rrd=smeeData.Rrd,
    Rrq=smeeData.Rrq,
    TrRef=smeeData.TrRef,
    phiMechanical(start=0),
    m=m,
    Rs=smeeData.Rs*m/3,
    Lssigma=smeeData.Lssigma*m/3,
    Lmd=smeeData.Lmd*m/3,
    Lmq=smeeData.Lmq*m/3,
    TsOperational=293.15,
    alpha20s=smeeData.alpha20s,
    effectiveStatorTurns=smeeData.effectiveStatorTurns,
    alpha20r=smeeData.alpha20r,
    TrOperational=293.15,
    VsNominal=100,
    IeOpenCircuit=1,
    Re=10) annotation (Placement(transformation(extent={{-10,40},{10,60}})));

  Modelica.Mechanics.Rotational.Sources.ConstantSpeed constantSpeed(
    useSupport=false,
    w_fixed(displayUnit="rad/s") = 2*pi*smeeData.fsNominal/smeeData.p,
    phi(start=0, fixed=true)) annotation (Placement(transformation(
          extent={{80,-10},{60,10}})));
  parameter
    Modelica.Electrical.Machines.Utilities.ParameterRecords.SM_ElectricalExcitedData
    smeeData(effectiveStatorTurns=10,
             useDamperCage=false) "Machine data"
    annotation (Placement(transformation(extent={{70,72},{90,92}})));
  Modelica.Electrical.QuasiStationary.MultiPhase.Basic.Star starQS(m=m)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-20,70})));
  Modelica.Electrical.QuasiStationary.SinglePhase.Basic.Ground groundQS
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-50,70})));
  Modelica.Electrical.QuasiStationary.MultiPhase.Sensors.PotentialSensor
    potentialSensorQS(m=m)
    annotation (Placement(transformation(extent={{10,60},{30,80}})));
  Modelica.Magnetic.FundamentalWave.BasicMachines.SynchronousInductionMachines.SM_PermanentMagnet
    smpm(
    p=smeeData.p,
    fsNominal=smeeData.fsNominal,
    TsRef=smeeData.TsRef,
    Jr=smeeData.Jr,
    Js=smeeData.Js,
    frictionParameters=smeeData.frictionParameters,
    statorCoreParameters=smeeData.statorCoreParameters,
    strayLoadParameters=smeeData.strayLoadParameters,
    useDamperCage=smeeData.useDamperCage,
    Lrsigmad=smeeData.Lrsigmad,
    Lrsigmaq=smeeData.Lrsigmaq,
    Rrd=smeeData.Rrd,
    Rrq=smeeData.Rrq,
    TrRef=smeeData.TrRef,
    m=m,
    Rs=smeeData.Rs*m/3,
    Lssigma=smeeData.Lssigma*m/3,
    Lszero=smeeData.Lszero*m/3,
    Lmd=smeeData.Lmd*m/3,
    Lmq=smeeData.Lmq*m/3,
    TsOperational=293.15,
    alpha20s=smeeData.alpha20s,
    effectiveStatorTurns=smeeData.effectiveStatorTurns,
    alpha20r=smeeData.alpha20r,
    TrOperational=293.15) annotation (Placement(transformation(extent={
            {-10,-50},{10,-30}})));

  Modelica.Electrical.MultiPhase.Basic.Star star(m=m) annotation (
      Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=180,
        origin={-20,-20})));
  Modelica.Electrical.Analog.Basic.Ground ground annotation (Placement(
        transformation(
        extent={{-10,-10},{10,10}},
        rotation=270,
        origin={-50,-20})));
  Modelica.Electrical.MultiPhase.Sensors.PotentialSensor
    potentialSensor(m=m)
    annotation (Placement(transformation(extent={{10,-30},{30,-10}})));
  Modelica.Electrical.QuasiStationary.MultiPhase.Basic.Resistor
    resistorQS(m=m, R_ref=fill(1e6*m/3, m))
    annotation (Placement(transformation(extent={{-10,70},{10,90}})));
  Modelica.Electrical.Analog.Sources.RampCurrent rampCurrent(I=2, duration=1)
    annotation (Placement(transformation(
        extent={{-10,-10},{10,10}},
        rotation=90,
        origin={-30,40})));
  Modelica.Electrical.Analog.Basic.Ground ground1
    annotation (Placement(transformation(extent={{-20,0},{0,20}})));
equation
  connect(starQS.plug_p, smpmQS.plug_sn) annotation (Line(
      points={{-10,70},{-10,60},{-6,60}},
      color={85,170,255}));
  connect(groundQS.pin, starQS.pin_n) annotation (Line(
      points={{-40,70},{-30,70}},
      color={85,170,255}));
  connect(potentialSensorQS.plug_p, smpmQS.plug_sp) annotation (Line(
      points={{10,70},{10,60},{6,60}},
      color={85,170,255}));
  connect(constantSpeed.flange, smpm.flange) annotation (Line(
      points={{60,0},{50,0},{50,-40},{10,-40}}));
  connect(constantSpeed.flange, smpmQS.flange) annotation (Line(
      points={{60,0},{50,0},{50,50},{10,50}}));
  connect(ground.p, star.pin_n) annotation (Line(
      points={{-40,-20},{-30,-20}},
      color={0,0,255}));
  connect(star.plug_p, smpm.plug_sn) annotation (Line(
      points={{-10,-20},{-10,-30},{-6,-30}},
      color={0,0,255}));
  connect(potentialSensor.plug_p, smpm.plug_sp) annotation (Line(
      points={{10,-20},{10,-30},{6,-30}},
      color={0,0,255}));
  connect(resistorQS.plug_p, smpmQS.plug_sn) annotation (Line(
      points={{-10,80},{-10,60},{-6,60}},
      color={85,170,255}));
  connect(resistorQS.plug_n, smpmQS.plug_sp) annotation (Line(
      points={{10,80},{10,60},{6,60}},
      color={85,170,255}));
  connect(rampCurrent.n, smpmQS.pin_ep)
    annotation (Line(points={{-30,50},{-30,56},{-10,56}}, color={0,0,255}));
  connect(rampCurrent.p, smpmQS.pin_en) annotation (Line(points={{-30,30},{-30,26},
          {-10,26},{-10,44}}, color={0,0,255}));
  connect(ground1.p, smpmQS.pin_en)
    annotation (Line(points={{-10,20},{-10,44}}, color={0,0,255}));
  annotation (
    experiment(
      Interval=0.001,
      Tolerance=1e-06,
      __Dymola_Algorithm="Dassl"),
    Documentation(info="<html>
<p>
This example compares a time transient and a quasi static model of a permanent magnet synchronous machine.
The machines are operated at constant mechanical angular velocity.</p>

<p>
Simulate for 0.1 second and plot (versus time):
</p>

<ul>
<li><code>potentialSenor.phi|potentialSensorQS.abs_y[1]</code>: potential of terminal</li>
</ul>

<h5>Note</h5>
<p>The resistors connected to the terminals of the windings of the quasi static machine model are necessary to numerically stabilize the simulation.</p>
</html>"),
    Diagram(coordinateSystem(preserveAspectRatio=false, extent={{-100,-100},
            {100,100}}), graphics={
        Text(
          extent={{-100,8},{-20,0}},
                  textStyle={TextStyle.Bold},
          textString="%m phase quasi static"),               Text(
                  extent={{-100,-92},{-20,-100}},
                  fillColor={255,255,170},
                  fillPattern=FillPattern.Solid,
                  textStyle={TextStyle.Bold},
                  textString="%m phase transient")}));
end SMPM_OpenCircuit;
