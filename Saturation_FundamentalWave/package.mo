within ;
package Saturation_FundamentalWave "Fundamental wave machine models considering saturation"
extends Modelica.Icons.Package;

annotation (uses(Modelica(version="3.2.3"), Complex(version="4.0.0")));
end Saturation_FundamentalWave;
